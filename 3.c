#include<stdio.h>
int main()
{
    int a[5][5],b[5][5],c[5][5]={0},d[5][5]={0};
    int i,j,k,m,n,p,q;
    printf("Enter number of rows and columns in first matrix : ");
    scanf("%d %d",&m,&n);
    printf("Enter number of rows and columns in second matrix : ");
    scanf("%d %d",&p,&q);
    if(m!=p || n!=q)
    {
        printf("Matrix addition is not possible");
        return;
    }
    else if(n!=p)
    {
        printf("Matrix multiplication is not possible");
        return;
    }
    else
    {
        printf("Enter elements of first matrix : \n");
        for(i=0;i<m;i++)
        {
            for(j=0;j<n;j++)
            {
                scanf("%d",&a[i][j]);
            }
        }
        printf("Enter elements of second matrix : \n");
        for(i=0;i<p;i++)
        {
            for(j=0;j<q;j++)
            {
                scanf("%d",&b[i][j]);
            }
        }
        printf(" \n");

        for(i=0;i<m;i++)
            for(j=0;j<n;j++)
            c[i][j] = a[i][j] + b[i][j];
        printf("Result of matrix addition : \n");
        for(i=0;i<m;i++)
        {
            for(j=0;j<n;j++)
                printf("%d",c[i][j]);
            printf(" \n");
        }

        for(i=0;i<m;i++)
        {
            for(j=0;j<q;j++)
            {
                for(k=0;k<p;k++)
                {
                    d[i][j] += a[i][k]*b[k][j];
                }
            }
        }
        printf("Result of matrix multiplication : \n");
        for(i=0;i<m;i++)
        {
            for(j=0;j<q;j++)
                printf("%d",d[i][j]);
            printf(" \n");
        }



    }
}
